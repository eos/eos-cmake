#!/usr/bin/env bash

echo "Exporting STCI_ROOT_PATH=/eos/project/s/storage-ci/www/eos"
export STCI_ROOT_PATH="/eos/project/s/storage-ci/www/eos"
echo "Exporting EOS_CODENAME=diopside"
export EOS_CODENAME="diopside"

for BUILD_TYPE in "el-7" "el-8" "el-8s"; do
    EXPORT_DIR_RPMS=${STCI_ROOT_PATH}/${EOS_CODENAME}-depend/${BUILD_TYPE}/x86_64/
    EXPORT_DIR_SRPMS=${STCI_ROOT_PATH}/${EOS_CODENAME}-depend/${BUILD_TYPE}/SRPMS/
    echo "Publishing for: ${BUILD_TYPE} in location: ${EXPORT_DIR_RPMS}"
    mkdir -p ${EXPORT_DIR_RPMS}
    mkdir -p ${EXPORT_DIR_SRPMS}
    cp -n ${BUILD_TYPE}_artifacts/SRPMS/*.src.rpm ${EXPORT_DIR_SRPMS}
    cp -n ${BUILD_TYPE}_artifacts/RPMS/x86_64/*.rpm ${EXPORT_DIR_RPMS}
    createrepo -q ${EXPORT_DIR_RPMS}
done
