# Description

This project contains the `rpmbuild` directory structure to create the `eos-cmake` rpm file.

# How to build the `eos-cmake` RPM

Make sure you have installed `rpmbuild` and `openssl-devel`. Otherwise, install them:

```bash
sudo yum install -y rpm-build openssl-devel
```

Once this is done, just go to the root directory of this project and type:

```
rpmbuild --define "_topdir `pwd`" -bb SPECS/eos-cmake.spec
```
