%define _prefix /opt/eos/cmake
%define qa_rpaths 3

Name:           eos-cmake
Version:        3.19.7
Release:        1%{?dist}
Summary:        CMake

License: BSD and MIT and zlib
URL: http://www.cmake.org

BuildRequires: openssl-devel

Source0: ~/rpmbuild/SOURCES/cmake-3.19.7.tar.gz
%description
CMake is used to control the software compilation process using simple
platform and compiler independent configuration files. CMake generates
native makefiles and workspaces that can be used in the compiler
environment of your choice. CMake is quite sophisticated: it is possible
to support complex environments requiring system configuration, preprocessor
generation, code generation, and template instantiation

%global debug_package %{nil}

%prep
%autosetup -n cmake-3.19.7


%build
./configure --parallel=20 --prefix=%{_prefix}


%install
export QA_RPATHS=%{qa_rpaths}
rm -rf $RPM_BUILD_ROOT
%make_install -j 20

%files
%defattr(-,root,root,-)
/opt/eos/cmake/bin
/opt/eos/cmake/doc
/opt/eos/cmake/share

%changelog
* Tue Mar 16 2021 root
-
